package com.example.demo.models.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryRequestParams {

    @NotNull
    private String name;
    @NotNull
    private Long continentId;

}
