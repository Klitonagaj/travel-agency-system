package com.example.demo.models.requests;

import com.example.demo.entities.TourEntity;
import com.example.demo.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchasingATourRequestParams {

    @NonNull
    private TourEntity tourId;

    @NonNull
    private int adultsSeats;

    @NonNull
    private int childrenSeats;

    @NonNull
    private UserEntity userId;
}
