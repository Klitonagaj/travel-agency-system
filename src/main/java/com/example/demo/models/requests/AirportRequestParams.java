package com.example.demo.models.requests;

import com.example.demo.entities.CityEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AirportRequestParams {

    @NotNull
    private String name;

    @NotNull
    private Long cityId;

}
