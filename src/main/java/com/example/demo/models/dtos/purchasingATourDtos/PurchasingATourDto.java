package com.example.demo.models.dtos.purchasingATourDtos;

import com.example.demo.entities.TourEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchasingATourDto {

    private Long tourId;
    private int adultsSeats;
    private int childrenSeats;
    private Long userId;
}
