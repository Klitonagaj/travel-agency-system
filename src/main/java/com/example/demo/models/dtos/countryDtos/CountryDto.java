package com.example.demo.models.dtos.countryDtos;

import javax.annotation.Resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryDto {
    private String name;
    private String continentName;

}
