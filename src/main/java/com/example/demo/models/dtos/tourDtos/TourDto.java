package com.example.demo.models.dtos.tourDtos;

import com.example.demo.entities.AirportEntity;
import com.example.demo.entities.CityEntity;
import com.example.demo.entities.HotelEntity;
import com.example.demo.entities.PurchasingATourEntity;
import com.example.demo.enumeration.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TourDto {

    private Long cityId;
    private AirportEntity airportWhereFrom;

    private CityEntity cityWhereTo;
    private AirportEntity airportWhereTo;

    private List<PurchasingATourEntity> purchasingATourList;

    private HotelEntity hotel;

    private LocalDateTime departureDate;
    private int numberOfDays;

    private double priceForAdult;
    private double priceForChild;

    private boolean isPromoted;

    private int numberOfAdultSeats;
    private int numberOfPlacesForChild;

    private Type type ;
}
