package com.example.demo.controllers;



import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.UserRequestParams;
import com.example.demo.services.IUserService;
import com.example.demo.services.impl.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private IUserService iUserService;
    @Autowired
    private UserServiceImp userServiceImp;

    @PostMapping(value = "/addUser")
    public ResponseEntity <UserDto>  createNewUser (@RequestBody UserRequestParams userRequestParams){
    var userDto = (UserDto) iUserService.save(userRequestParams);
        return ResponseEntity.ok(userDto);
    }

    @GetMapping("/users")
    public ResponseEntity <List<UserDto>>  getUser (){
        final List<UserDto> userDtoList = iUserService.read();
        return ResponseEntity.ok(userDtoList);
    }

    @DeleteMapping("/user/{userId}")
    public void  deleteUser (@PathVariable("userId") Long userId) {
        iUserService.delete(userId);
    }

    @PutMapping("/user{userId}")
    public ResponseEntity<UserDto>  updateUser (@PathVariable long userId,@RequestBody UserRequestParams userRequestParams){
        UserDto userDto =(UserDto) iUserService.update(userId,userRequestParams);
        return ResponseEntity.ok(userDto);
    }

    @GetMapping("users/")
    public ResponseEntity <List<UserDto>> filterUser(@RequestParam String firstName,
                                                     @RequestParam String lastName){
        var userFilterDto= iUserService.filter(firstName,lastName);
        return  ResponseEntity.ok(userFilterDto);
    }


}
