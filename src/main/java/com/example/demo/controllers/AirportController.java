package com.example.demo.controllers;

import com.example.demo.models.dtos.airportDtos.AirportDto;
import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.AirportRequestParams;
import com.example.demo.models.requests.UserRequestParams;
import com.example.demo.services.IAirportService;
import com.example.demo.services.impl.AirportServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class AirportController {

    @Autowired
    private IAirportService iAirportService;
    @Autowired
    private AirportServiceImp airportServiceImp;


    @PostMapping(value = "/addAirport")
    public ResponseEntity<AirportDto> createNewAirport (@RequestBody AirportRequestParams airportRequestParams){
        var airportDto = (AirportDto) iAirportService.save(airportRequestParams);
        return ResponseEntity.ok(airportDto);
    }

    @GetMapping("/airport")
    public ResponseEntity <List<AirportDto>>  getAirport (){
        final List<AirportDto> airportDtoList = iAirportService.read();
        return ResponseEntity.ok(airportDtoList);
    }

}
