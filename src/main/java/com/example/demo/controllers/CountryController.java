package com.example.demo.controllers;



import com.example.demo.models.dtos.countryDtos.CountryDto;
import com.example.demo.models.requests.CountryRequestParams;
import com.example.demo.services.ICountryService;
import com.example.demo.services.impl.CountryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CountryController {

    @Autowired
    private ICountryService iCountryService;
    @Autowired
    private CountryServiceImp countryServiceImp;

    @PostMapping(value = "/addCountry")
    public ResponseEntity <CountryDto>  createNewUser (@RequestBody CountryRequestParams countryRequestParams){
        var countryDto = (CountryDto) iCountryService.save(countryRequestParams);
        return ResponseEntity.ok(countryDto);
    }

    @GetMapping("/country")
    public ResponseEntity <List<CountryDto>>  getCountry (){
        final List<CountryDto> countryDtoList = iCountryService.read();
        return ResponseEntity.ok(countryDtoList);
    }

    @DeleteMapping("/country/{countryId}")
    public void  deleteCountry (@PathVariable("countryId") Long countryId) {
        iCountryService.delete(countryId);
    }

    @PutMapping("/country/{countryId}")
    public ResponseEntity<CountryDto>  updateCountry (@PathVariable long countryId,@RequestBody CountryRequestParams countryRequestParams){
        CountryDto countyDto =(CountryDto) iCountryService.update(countryId, countryRequestParams);
        return ResponseEntity.ok(countyDto);
    }

//    @GetMapping("country/")
//    public ResponseEntity <List<CountryDto>> filterCountry(@RequestParam String name){
//        var userFilterDto= iUserService.filter(firstName,lastName);
//        return  ResponseEntity.ok(userFilterDto);
//    }


}
