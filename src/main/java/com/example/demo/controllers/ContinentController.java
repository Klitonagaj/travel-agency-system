package com.example.demo.controllers;

import com.example.demo.entities.ContinentEntity;
import com.example.demo.models.dtos.airportDtos.AirportDto;
import com.example.demo.models.dtos.continentDtos.ContinentDto;
import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.AirportRequestParams;
import com.example.demo.models.requests.ContinentRequestParams;
import com.example.demo.models.requests.UserRequestParams;
import com.example.demo.services.IContinentService;
import com.example.demo.services.impl.ContinentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ContinentController {

    @Autowired
    private ContinentServiceImp continentServiceImp;

    @Autowired
    private IContinentService iContinentService;

    @PostMapping(value = "/addContinent")
    public ResponseEntity<ContinentDto> createNewContinent (@RequestBody ContinentRequestParams continentRequestParams){
        var continentDto = (ContinentDto) iContinentService.save(continentRequestParams);
        return ResponseEntity.ok(continentDto);
    }

    @GetMapping("/readContinent")
    public ResponseEntity <List<ContinentDto>>  getContinent (){

        final List<ContinentDto> continentDtoList = iContinentService.read();
        return ResponseEntity.ok(continentDtoList);
    }

    @DeleteMapping("/continent/{continentId}")
    public void  deleteContinent (@PathVariable("continentId") Long continentId) {
        iContinentService.delete(continentId);
    }

    @PutMapping("/continent{continentId}")
    public ResponseEntity<ContinentDto>  updateContinent (@PathVariable long continentId, @RequestBody ContinentRequestParams continentRequestParams){
        ContinentDto continentDto =(ContinentDto) iContinentService.update(continentId, continentRequestParams);
        return ResponseEntity.ok(continentDto);
    }





}
