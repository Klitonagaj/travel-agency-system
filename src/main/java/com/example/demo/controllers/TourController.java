package com.example.demo.controllers;

import com.example.demo.models.dtos.tourDtos.TourDto;
import com.example.demo.models.requests.TourRequestParams;
import com.example.demo.services.ITourService;

import com.example.demo.services.impl.TourServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class TourController {

    @Autowired
    private TourServiceImp tourServiceImp;

    @PostMapping(value ="/addTour")
    public ResponseEntity<TourDto> creatNewTour (@RequestBody TourRequestParams tourRequestParams){
        var tourDto = (TourDto) tourServiceImp.save(tourRequestParams);
        return ResponseEntity.ok(tourDto);
    }


}
