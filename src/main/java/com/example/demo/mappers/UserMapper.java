package com.example.demo.mappers;

import com.example.demo.entities.UserEntity;
import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.UserRequestParams;

import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto convertUserToDto(UserEntity userEntity);
    List<UserDto> convertUsersToDtoList(List<UserEntity> userEntityList);
    UserEntity convertToEntity(UserRequestParams userParameterRequest);

}
