package com.example.demo.mappers;

import com.example.demo.entities.PurchasingATourEntity;
import com.example.demo.entities.UserEntity;
import com.example.demo.models.dtos.purchasingATourDtos.PurchasingATourDto;
import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.PurchasingATourRequestParams;
import com.example.demo.models.requests.UserRequestParams;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PurchasingATourMapper {


    PurchasingATourDto convertPurchasingATourToDto(PurchasingATourEntity purchasingATourEntity);
    List<PurchasingATourDto> convertPurchasingATourToDtoList(List<PurchasingATourEntity> purchasingATourEntityList);
    PurchasingATourEntity covertToPurchasingATourEntity(PurchasingATourRequestParams purchasingATourRequestParams);


}
