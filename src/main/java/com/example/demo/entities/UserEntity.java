package com.example.demo.entities;

import lombok.*;
/*
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
*/

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class UserEntity extends BaseEntity{


    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private String passportId;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<RoleEntity> roles;


    @OneToMany(mappedBy = "user")
    private List<PurchasingATourEntity> purchasingATourList;



}

