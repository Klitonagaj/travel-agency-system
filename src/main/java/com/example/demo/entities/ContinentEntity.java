package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "continent")
public class ContinentEntity extends BaseEntity {


    @Column(name = "continent_name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "continent")
    private List<CountryEntity> country;


}
