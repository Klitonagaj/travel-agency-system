package com.example.demo.entities;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "country")
public class CountryEntity extends BaseEntity  {


    @Column(name = "country_name", length=50, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "continent_id")
    private ContinentEntity continent;

    @OneToMany(mappedBy = "country")
    private List<CityEntity> city;

}
