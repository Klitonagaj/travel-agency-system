package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="airport")
public class AirportEntity extends BaseEntity {

    @Column(name = "airport_name", length=50, nullable = false)
    private String name;

    @OneToMany(mappedBy ="airportWhereFrom")
    private List<TourEntity> tourListWhereFrom;

    @OneToMany(mappedBy = "airportWhereTo")
    private List<TourEntity> tourListWhereTo;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private CityEntity city;

}
