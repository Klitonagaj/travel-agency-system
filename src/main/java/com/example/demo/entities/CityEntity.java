package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "city")
public class CityEntity extends BaseEntity{

    @Column(name = "city", length=50, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryEntity country;



    @OneToMany(mappedBy = "city")
    private List<AirportEntity> airport;

    @OneToMany(mappedBy = "designatedCity")
    private List<HotelEntity> hotel;


    @OneToMany(mappedBy = "cityWhereFrom")
    private List<TourEntity> tourListWhereFrom;

    @OneToMany(mappedBy = "cityWhereTo")
    private List<TourEntity> tourListWhereTo;

}
