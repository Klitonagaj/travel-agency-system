package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hotel")
public class HotelEntity extends BaseEntity {

    @Column(name = "hotel_name", length=50, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "designatedCityId")
    private CityEntity designatedCity;

    @OneToMany(mappedBy = "hotel")
    private List<TourEntity> tour;

    private int standart;

    private String description;


}
