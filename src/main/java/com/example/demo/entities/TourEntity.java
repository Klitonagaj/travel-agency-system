package com.example.demo.entities;

import com.example.demo.enumeration.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tour")
public class TourEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "cityWhereFromId")
    private CityEntity cityWhereFrom;

    @ManyToOne
    @JoinColumn(name = "airportWhereFromId")
    private AirportEntity airportWhereFrom;

    @ManyToOne
    @JoinColumn(name = "cityWhereToId")
    private CityEntity cityWhereTo;

    @ManyToOne
    @JoinColumn(name = "airportWhereToId")
    private AirportEntity airportWhereTo;

    @OneToMany(mappedBy = "tour")
    private List<PurchasingATourEntity> purchasingATourList;

    @ManyToOne
    @JoinColumn(name = "hotelWhereToId")
    private HotelEntity hotel;

    private LocalDateTime departureDate;
    private int numberOfDays;

    private double priceForAdult;
    private double priceForChild;

    private boolean isPromoted;

    private int numberOfAdultSeats;
    private int numberOfPlacesForChild;

    private Type type;

}
