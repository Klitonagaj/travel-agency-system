package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "purchasing_a_tour")
public class PurchasingATourEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "tour_id")
    private TourEntity tour;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    private int numberOfAdultsSeatsBought;

    private int numberOfChildreenSeatsBought;

    private String data_of_participants;

    private double amount;

}
