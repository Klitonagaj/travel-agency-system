package com.example.demo.services;

import com.example.demo.models.dtos.purchasingATourDtos.PurchasingATourDto;
import com.example.demo.models.dtos.userDtos.UserDto;

import java.util.List;

public interface IPurchasingATour<T, R> extends BaseService<T,R> {


    List<R> filterBy (final Long tourId,
                      final int adultsSeats,
                      final int childrenSeats,
                      final Long userId );

}
