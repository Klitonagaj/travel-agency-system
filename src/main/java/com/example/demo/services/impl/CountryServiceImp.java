package com.example.demo.services.impl;

import com.example.demo.entities.CountryEntity;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.dtos.countryDtos.CountryDto;
import com.example.demo.models.requests.CountryRequestParams;
import com.example.demo.repositories.ContinentRepository;
import com.example.demo.repositories.CountryRepository;
import com.example.demo.services.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
public class CountryServiceImp implements ICountryService<CountryRequestParams, CountryDto> {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ContinentRepository continentRepository;


    @Override
    public CountryDto save(final CountryRequestParams countryRequestParams){

        var continent = continentRepository.findById(countryRequestParams.getContinentId())
                .orElseThrow(() -> new NotFoundException("Continent id not valid"));
        var countryEntity= CountryEntity.builder()
                .name(countryRequestParams.getName())
                .continent(continent)
                .build();

        countryRepository.save(countryEntity);
        return CountryDto.builder()
                .name(countryEntity.getName())
                .continentName(countryEntity.getContinent().getName())
                .build();
    }


    @Override
    public CountryDto update(Long id, CountryRequestParams countryRequestParams) {

        var continent = continentRepository.findById(countryRequestParams.getContinentId())
                .orElseThrow(() -> new NotFoundException("Continent id not valid"));
        var countryEntity = countryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Country id not valid"));
        countryEntity.setName(countryRequestParams.getName());
        countryEntity.setContinent(continent);

        countryRepository.save(countryEntity);
        return CountryDto.builder()
                .name(countryEntity.getName())
                .continentName(countryEntity.getContinent().getName())
                .build();
    }

    @Override
    public void delete(Long id) {
        countryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Country id not valid"));
        countryRepository.deleteById(id);
    }


    @Override
    public List<CountryDto> read() {
        var countryEntitiesList = countryRepository.findAll();
        return countryEntitiesList.stream().map((countryEntity) ->
                CountryDto.builder()
                        .name(countryEntity.getName())
                        .continentName(countryEntity.getContinent().getName())
                        .build()
        ).collect(Collectors.toList());
    }
}
