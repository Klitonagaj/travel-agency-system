package com.example.demo.services.impl;

import com.example.demo.entities.UserEntity;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.mappers.UserMapper;
import com.example.demo.models.CurrentUser;
import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.UserRequestParams;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.IUserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImp implements IUserService<UserRequestParams, UserDto>/* , UserDetailsService*/ {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDto save(final UserRequestParams userRequestParams) {
        var userEntity =
                UserEntity.builder()
                        .firstName(userRequestParams.getFirstName())
                        .lastName(userRequestParams.getLastName())
                        .email(userRequestParams.getEmail())
                        .username(userRequestParams.getUsername())
                        .password(userRequestParams.getPassword())
                        .passportId(userRequestParams.getPassportId())
                        .build();
        userRepository.save(userEntity);
        return UserDto.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .firstName(userEntity.getFirstName())
                .email(userEntity.getEmail())
                .lastName(userEntity.getLastName())
                .passportId(userEntity.getPassportId())
                .build();
    }

    @Override
    public CurrentUser getUserIdByUsername(String username) {
        return userRepository.getCurrentUserByUsername(username);
    }

    @Override
    public UserDto update(final Long id, UserRequestParams userRequestParams) {
        var userEntity = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        userEntity.setUsername(userRequestParams.getUsername());
        userEntity.setEmail(userRequestParams.getEmail());
        userEntity.setFirstName(userRequestParams.getFirstName());
        userEntity.setLastName(userRequestParams.getLastName());
        userEntity.setPassword(userRequestParams.getPassword());
        userEntity.setPassportId(userRequestParams.getPassportId());
        userRepository.save(userEntity);
        return UserDto.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .firstName(userEntity.getFirstName())
                .email(userEntity.getEmail())
                .lastName(userEntity.getLastName())
                .passportId(userEntity.getPassportId())
                .build();
    }

    @Override
    public void delete(Long id) {
        userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        userRepository.deleteById(id);
    }

    @Override
    public List<UserDto> read() {
        var userEntitiesList = userRepository.findAll();
        return userEntitiesList.stream().map((userEntity) ->
                UserDto.builder()
                        .id(userEntity.getId())
                        .username(userEntity.getUsername())
                        .firstName(userEntity.getFirstName())
                        .email(userEntity.getEmail())
                        .lastName(userEntity.getLastName())
                        .passportId(userEntity.getPassportId())
                        .build()
        ).collect(Collectors.toList());
    }

    @Override
    public List<UserDto> filter(final String firstName, final String lastName) {
        var usersList = userRepository.findByFirstNameAndLastName(firstName, lastName);
        return usersList.stream().map((userEntity) ->
                UserDto.builder()
                        .id(userEntity.getId())
                        .username(userEntity.getUsername())
                        .firstName(userEntity.getFirstName())
                        .email(userEntity.getEmail())
                        .lastName(userEntity.getLastName())
                        .passportId(userEntity.getPassportId())
                        .build()
        ).collect(Collectors.toList());
    }

   /* @Override
    public UserDetails loadUserByUsername(String username) {
        return null;
    }*/


}
