package com.example.demo.services.impl;

import com.example.demo.entities.AirportEntity;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.dtos.airportDtos.AirportDto;
import com.example.demo.models.dtos.userDtos.UserDto;
import com.example.demo.models.requests.AirportRequestParams;
import com.example.demo.repositories.AirportRepository;
import com.example.demo.repositories.CityRepository;
import com.example.demo.services.IAirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AirportServiceImp implements IAirportService<AirportRequestParams, AirportDto> {

    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private CityRepository cityRepository;

    @Override
    public AirportDto save(AirportRequestParams airportRequestParams) {

        var city = cityRepository.findById(airportRequestParams.getCityId())
                .orElseThrow(() -> new NotFoundException("Airport id not valid"));

        var airportEntity = AirportEntity.builder()
                .name(airportRequestParams.getName())
                .city(city)
                .build();
        airportRepository.save(airportEntity);
        return AirportDto.builder()
                .name(airportEntity.getName())
                .city(airportEntity.getCity().getName())
                .build();
    }

    @Override
    public AirportDto update(Long id, AirportRequestParams request) {

        var city = cityRepository.findById(request.getCityId())
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        var airportEntity = airportRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        airportEntity.setName(request.getName());
        airportEntity.setCity(city);

        airportRepository.save(airportEntity);
        return AirportDto.builder()
                .name(airportEntity.getName())
                .city(airportEntity.getCity().getName())
                .build();
    }

    @Override
    public void delete(Long id) {
        airportRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User id not valid"));
        airportRepository.deleteById(id);

    }

    @Override
    public List<AirportDto> read() {

       var airportEntityList = airportRepository.findAll();

        return airportEntityList.stream().map((airportEntity) ->
                AirportDto.builder()
                        .name(airportEntity.getName())
                        .city(airportEntity.getCity().getName())
                        .build()
                ).collect(Collectors.toList());
    }

}
