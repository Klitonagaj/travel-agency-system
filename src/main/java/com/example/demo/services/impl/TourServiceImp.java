package com.example.demo.services.impl;

import com.example.demo.entities.TourEntity;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.dtos.tourDtos.TourDto;
import com.example.demo.models.requests.TourRequestParams;
import com.example.demo.repositories.TourRepository;
import com.example.demo.services.ITourService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TourServiceImp implements ITourService<TourRequestParams, TourDto> {

    @Autowired
    private TourRepository tourRepository;


    @Override
    public TourDto save(TourRequestParams tourRequestParams) {
        return null;
    }

    @Override
    public TourDto update(Long id, TourRequestParams request) {

        return null;
    }

    @Override
    public void delete(Long id) {
        tourRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("tour id not valid"));
        tourRepository.deleteById(id);
    }

    @Override
    public List<TourDto> read() {

        return null;
    }
}
