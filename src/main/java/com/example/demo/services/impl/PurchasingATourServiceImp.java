package com.example.demo.services.impl;

import com.example.demo.mappers.PurchasingATourMapper;
import com.example.demo.models.dtos.purchasingATourDtos.PurchasingATourDto;
import com.example.demo.models.requests.PurchasingATourRequestParams;
import com.example.demo.repositories.PurchasingATourRepository;
import com.example.demo.services.IPurchasingATour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PurchasingATourServiceImp implements IPurchasingATour<PurchasingATourRequestParams, PurchasingATourDto> {

    @Autowired
    private PurchasingATourRepository purchasingATourRepository;

    @Autowired
    private PurchasingATourMapper purchasingATourMapper;

    @Override
    public PurchasingATourDto save(PurchasingATourRequestParams purchasingATourRequestParams) {
        return null;
    }

    @Override
    public PurchasingATourDto update(Long id, PurchasingATourRequestParams request) {
        return null;
    }

    @Override
    public void delete(Long id) {  }

    @Override
    public List<PurchasingATourDto> read() {
        return null;
    }


    @Override
    public List<PurchasingATourDto> filterBy(final Long tourId, final int adultsSeats, final int childrenSeats, final Long userId ){

        return null;
  }


}
