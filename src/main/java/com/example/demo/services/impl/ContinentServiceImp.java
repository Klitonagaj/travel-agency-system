package com.example.demo.services.impl;

import com.example.demo.entities.ContinentEntity;
import com.example.demo.entities.UserEntity;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.models.dtos.continentDtos.ContinentDto;
import com.example.demo.models.requests.ContinentRequestParams;
import com.example.demo.repositories.ContinentRepository;
import com.example.demo.services.IContinentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ContinentServiceImp implements IContinentService<ContinentRequestParams, ContinentDto> {

    @Autowired
    private ContinentRepository continentRepository;

    @Override
    public ContinentDto save(ContinentRequestParams continentRequestParams) {

       var continentEntity =ContinentEntity.builder()
                .name(continentRequestParams.getName())
                .build();
       continentRepository.save(continentEntity);

        return ContinentDto.builder()
                .name(continentEntity.getName())
                .build();
    }

    @Override
    public ContinentDto update(Long id, ContinentRequestParams request) {

        var continentEntity = continentRepository.findById(id).
                orElseThrow(() -> new NotFoundException("Continent id not valid"));
        continentEntity.setName(request.getName());
        continentRepository.save(continentEntity);
        return ContinentDto.builder()
                .name(continentEntity.getName())
                .build();
    }

    @Override
    public void delete(Long id) {
        continentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Continent id not valid"));
        continentRepository.deleteById(id);

    }

    @Override
    public List<ContinentDto> read() {

      var continentEntityList=  continentRepository.findAll();
        return continentEntityList.stream().map( (continentEntity -> ContinentDto.builder()
                .name(continentEntity.getName())
                .build()) ).collect(Collectors.toList()) ;
    }
}
