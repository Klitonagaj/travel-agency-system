package com.example.demo.services;


import java.util.List;

public interface BaseService<T, R> {

    R save(T t);

    R update(final Long id, final T request);

    void delete(final Long id);

    List<R> read();
}
