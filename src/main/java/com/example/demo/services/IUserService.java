package com.example.demo.services;

import com.example.demo.models.CurrentUser;
import com.example.demo.models.dtos.userDtos.UserDto;
//import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;


public interface IUserService<T, R> extends BaseService<T, R> {


    List<UserDto> filter(final String firstName, final String lastName);

    //UserDetails loadUserByUsername(String username);

    CurrentUser getUserIdByUsername(String username);



}
