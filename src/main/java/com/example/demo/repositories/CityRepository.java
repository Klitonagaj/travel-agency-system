package com.example.demo.repositories;

import com.example.demo.entities.AirportEntity;
import com.example.demo.entities.CityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<CityEntity, Long> {
}
