package com.example.demo.repositories;

import com.example.demo.entities.CityEntity;
import com.example.demo.entities.ContinentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContinentRepository extends JpaRepository<ContinentEntity, Long> {
}
