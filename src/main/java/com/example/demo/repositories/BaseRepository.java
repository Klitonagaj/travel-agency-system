package com.example.demo.repositories;

import com.example.demo.entities.BaseEntity;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

public class BaseRepository <T>{
    @Autowired
    @Getter
    protected EntityManager entityManager;

    public BaseEntity save(BaseEntity entity) {
        this.entityManager.persist(entity);
        return entity;
    }
}
