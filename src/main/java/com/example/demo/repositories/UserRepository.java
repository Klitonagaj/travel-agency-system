package com.example.demo.repositories;

import com.example.demo.entities.UserEntity;
import com.example.demo.models.CurrentUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository <UserEntity, Long> {

    List<UserEntity> findByFirstNameAndLastName(String name, String surname);

    Optional<UserEntity> findByUsername(String username);
    @Query("select new com.example.demo.models.CurrentUser(u.id) from UserEntity u where u.username = :username")
    CurrentUser getCurrentUserByUsername(@Param("username") String username);


}
